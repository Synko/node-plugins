module.exports = {
	plugins: [
		'9gag',
		'calculadora',
		'callback',
		'coelho',
		'commits',
		'dado',
		'doge',
		'download',
		'dump',
		'echo',
		'example',
		'gif',
		'github',
		'help',
		'ip',
		'javascript',
		'latex',
		'lmgtfy',
		'perguntas',
		'ping',
		'sorteio',
		'text',
		//'torrent', //TODO: API OFF
		'xkcd'
	],
	defaultLang: 'pt',
	locales: [
		'pt',
		'en'
	]
}

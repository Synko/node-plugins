function inline(ctx) {
	var input = ctx.match[1]
	var results = []
	results.push({
		type: 'article',
		title: ctx._('Markdown Estilo'),
		id: 'textMarkdown',
		input_message_content: {
			// #i18n No remove '*'
			message_text: ctx._`*Texto estilo: Markdown*\n${input}`,
			parse_mode: 'Markdown'
		}
	})
	results.push({
		type: 'article',
		title: ctx._('HTML Estilo'),
		id: 'textHTML',
		input_message_content: {
			// #i18n No remove '<b>' or '</b>'
			message_text: ctx._`<b>Texto estilo: HTML</b>\n${input}`,
			parse_mode: 'HTML'
		}
	})
	results.push({
		type: 'article',
		title: ctx._('Bloco de Código'),
		id: 'textCode',
		input_message_content: {
			// #i18n No remove '<b>', '</b>', <code> and </code>
			message_text: ctx._`<b>Texto estilo: Bloco de Código</b>\n<code>${input}</code>`,
			parse_mode: 'HTML'
		}
	})
	//TODO: Add texto tachado
	ctx.answerInlineQuery(results, {
		cache_time: 0
	})
}

module.exports = {
	id: 'textformat',
	name: 'Texto Formate',
	about: 'Diversos estilos de formatação de textos.',
	regex: /^\/text[os]*\s(.+)/i,
	example: '/texto *LINDO*',
	classification: ['Ferramentas'],
	inline
}

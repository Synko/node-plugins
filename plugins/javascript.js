var axios = require('axios')

function base(ctx) {
	if (!ctx.privilege('admin')) {
		return false
	}
	var input = ctx.match[1]
	return `<b>Input</b>:
${input}
----------------
<b>Output</b>:
${eval(input)}
			`
}

function plugin(ctx) {
	return ctx.replyWithHTML(base(ctx))
}

function inline(ctx) {
	var output = base(ctx)
	return ctx.answerInlineQuery([
		{
			type: 'article',
			title: ctx._('Feito!'),
			id: 'javascript',
			input_message_content: {
				message_text: output,
				parse_mode: 'HTML'
			}
		}
	], {
		cache_time: 0
	})
}

module.exports = {
	id: 'javascript',
	name: 'JavaScript',
	about: 'Execute JavaScript',
	regex: /^\/javascript\s(.*)/i,
	example: '/javascript 1 + 1',
	classification: ['Bot', 'Ferramentas'],
	plugin,
	inline
}

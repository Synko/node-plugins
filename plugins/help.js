var similarity = require('similarity')

function selectPlugins(_, classification) {
	return _.classification.includes(classification)
}

function showClassifications(ctx) {
	// #18n 0 = (number)
	var text = ctx._`Temos um total de *${ctx.plugins.length}* plugins ativados.\n`
	text += ctx._('Toque em um botão para ver os *comandos relacionados*')
	var extra =  {
		parse_mode: 'Markdown',
		reply_markup: {
			inline_keyboard:
			[
				[{text: ctx._('🔨 Administração do Grupo'), callback_data: 'help:back'}], //GB Help
				[{text: ctx._('🕹 Entretenimento'), callback_data: 'helpbot:c:Entretenimento'}],
				[{text: ctx._('🔍 Pesquisa'), callback_data: 'helpbot:c:Pesquisa'}],
				[{text: ctx._('🛠 Ferramentas'), callback_data: 'helpbot:c:Ferramentas'}]
			]
		}
	}
	if (ctx.privilege('admin')) {
		extra.reply_markup.inline_keyboard.push([{
			text: ctx._('💎 Admin/Bot'), callback_data: 'helpbot:c:Bot'
		}])
	}
	if (ctx.updateType == 'callback_query') {
		return ctx.editMessageText(text, extra)
	} else if (ctx.chat.type == 'private') {
		return ctx.replyWithMarkdown(text, extra)
	} else {
		ctx.telegram.sendMessage(ctx.from.id, text, extra)
		.catch(e => {
			var _ = ctx._
			ctx.replyWithMarkdown(
				_('_Primeiro, Você precisa iniciar uma conversa comigo em uma mensagem privada._'), {
				reply_markup: {
					inline_keyboard:
					[[
						{
							text: _('🤖 Iniciar conversa.'),
							url: `https://telegram.me/${ctx.options.username}?start=help`
						}
					]]
				},
				disable_web_page_preview: true
			})
		})
		.then((e) => {
			if (e) {
				ctx.replyWithMarkdown(
					ctx._('_Eu enviei a mensagem de ajuda em uma mensagem privada._')
				)
			}
		})
		return
	}
}

function showInfoPlugin(ctx, _, n) {
	var isAdmin = ctx.privilege('admin')
	var output = ''
	if (n != 0) {
		output += `<b>${n}</b> - ${_.name}\n`
	} else {
		output += `<b>${ctx._(_.name)}</b>\n`
	}
	if (isAdmin) {
		output += `<b>Id:</b> <code>${_.id}</code>\n`
	}
	output += ctx._('<b>Sobre:</b>')
	output += ` ${ctx._(_.about)}\n`
	if (_.example) {
		output += ctx._('<b>Exemplo de uso:</b>')
		var exampleTxt = _.example.toString().replace(/,/g, ', ')
		output += ` ${ctx._(exampleTxt)}\n`
	}
	output += '\n'
	var classifications = []
	for (var classification of _.classification.sort()) {
		classifications.push(`<a href="https://telegram.me/${ctx.options.username}?start=help-c_${classification}">${ctx._(classification)}</a>`)
	}
	output += ctx._('<b>Categoria(s):</b>')
	output += ` ${classifications.toString().replace(/,/g, ', ')}\n`
	output += ctx._('<b>Compatilibidade:</b>') + ' '
	if (_.plugin) {
		output += ctx._('Modo de comando') + ', '
	}
	if (_.inline) {
		output += ctx._('Modo Inline')
	}
	output += '\n'
	if (isAdmin) {
		output += `<b>Regex:</b> <code>${_.regex.toString().replace(/,/g, ',\n')}</code>\n`
	}
	return output
}

function showPlugins(ctx) {
	var text = ctx._('<b>Escolha um plugin!</b>')
	var plugins = []
	var classification = ''
	if (ctx.updateType == 'message') {
		classification = ctx.match[1].replace('c_', '')
	} else {
		classification = ctx.match[3]
	}
	//TODO: Resumo simples de todos
	for (var _ of ctx.plugins) {
		if (selectPlugins(_, classification)) {
			if (ctx.match[4] && _.id == ctx.match[4]) {
				text = showInfoPlugin(ctx, _, 0)
				plugins.push([{text: `🔸 ${_.name}`, callback_data: `callback:same`}])
			} else {
				plugins.push([{text: `🔹 ${_.name}`, callback_data: `helpbot:c:${classification}:${_.id}`}])
			}
		}
	}

	var keyboard = plugins.reduce((total, next, index) => {
		if (index % 2 === 0) {
			total.push([])
		}
		total[total.length - 1].push(next[0])
		return total
	}, [])

	keyboard.push([{text: ctx._('🔙 Volta'), callback_data: 'helpbot:main'}])
	if (ctx.updateType == 'message' && ctx.chat.type == 'private') {
		return ctx.replyWithHTML(text, {
			reply_markup: {
				inline_keyboard: keyboard
			},
			disable_web_page_preview: true
		})
	} else {
		return ctx.editMessageText(text, {
			parse_mode: 'HTML',
			reply_markup: {
				inline_keyboard: keyboard
			},
			disable_web_page_preview: true
		})
	}
}

function plugin(ctx) {
	var match = ctx.match[1]
	if (match == 'help' || match == 'ajuda') {
		return showClassifications(ctx)
	} else if ([
		'c_Entretenimento',
		'c_Pesquisa',
		'c_Ferramentas',
		'c_Bot'
	].includes(match)) {
		return showPlugins(ctx)
	} else {
		return showClassifications(ctx)
	}
	return
}

function callback(ctx) {
	if (ctx.match[2] == 'main') {
		return showClassifications(ctx)
	} else if (ctx.match[2] == 'c' && ctx.match[3]) {
		return showPlugins(ctx)
	}
	return
}

function inline(ctx) {
	var results = []
	var input = ctx.match[1]
	var limit = 16
	if (input == 'help' || input == 'ajuda') {
		results.push({
			type: 'article',
			title: ctx._('Escrevar o nome do plugin!'),
			id: 'helpbot:start',
			input_message_content: {
				message_text: ctx._('Sem resultados! Escrevar o nome do plugin!')
			}
		})
	} else {
		for (var _ of ctx.plugins.sort((a, b) => {
			var indexA = a.name
			var indexB = b.name
			if (similarity(indexA, input) < similarity(indexB, input)) { return 1 }
			if (similarity(indexA, input) > similarity(indexB, input)) { return -1  }
			return 0
		})) {
			if (results.length < limit && !selectPlugins(_, 'Bot')) {
				results.push({
					type: 'article',
					title: `${_.name}`,
					id: `helpbot:id:${_.id}`,
					input_message_content: {
						message_text: showInfoPlugin(ctx, _, 0),
						parse_mode: 'HTML'
					}
				})
			}
		}
	}
	if (results.length == 0) {
		results.push({
			type: 'article',
			title: ctx._('Sem resultados!'),
			id: 'helpbot:start',
			input_message_content: {
				message_text: ctx._('Não achei nenhum plugin com esse nome!')
			}
		})
	}
	ctx.answerInlineQuery(results, {
		cache_time: 0
	})
}

module.exports = {
	id: 'helpbot',
	name: 'Ajuda',
	about: 'Mostrar informações de plugins e como usar.',
	regex: [
		/^\/ajuda[s]*\s(.+)/i,
		/^\/help\s(.+)/i,
		/^\/(ajuda)[s]*$/i,
		/^\/(help)$/i,
	],
	example: '/ajuda',
	classification: ['Ferramentas'],
	plugin,
	inline,
	callback
}

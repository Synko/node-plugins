function base(ctx) {
	if (!ctx.privilege('admin')) {
		return ctx._('Esse comando é somente para desenvolvedores do bot!')
	}
	return JSON.stringify(ctx.update, false, 4)
}

function plugin(ctx) {
	return ctx.reply(base(ctx))
}


module.exports = {
	id: 'dump',
	name: 'JSON Dump',
	about: 'Mostrar todas os dados de uma mensagem',
	regex: /^\/dump$/i,
	example: '/dump',
	classification: ['Bot', 'Ferramentas'],
	plugin
}

var download = require('download')

function plugin(ctx) {
	var match = ctx.match[1]
	ctx.replyWithDocument(`${match}`, {
		caption: ctx._`URL de origem do download: ${match}`,
	}).catch(() => {
		if (ctx.privilege('premium')) {
			download(`${match}`, false, {maxSize: 45000}).then((data) => {
				ctx.replyWithDocument({
					filename: `Download Beta (${Date()})`,
					source: Buffer.from(data, 'utf8')
				})
			}).catch(() => {
				ctx.reply(ctx._('❌ Erro: URL ou arquivo inválido!'))
			})
		} else {
			ctx.reply(ctx._('❌ Erro: URL ou arquivo inválido!'))
		}
	})
	return
}

function inline(ctx) {
	var match = ctx.match[1]
	ctx.answerInlineQuery([
		{
			type: 'document',
			title: ctx._('Download!'),
			id: 'download',
			caption: ctx._`URL de origem do download:${match}`,
			document_url: `${match}`
		}
	], {
		cache_time: 0
	}).catch(ctx.answerInlineQuery([
		{
			type: 'article',
			title: ctx._('❌ Erro: URL ou arquivo inválido!'),
			id: 'download:falid',
			input_message_content: {
				message_text: ctx._('❌ Erro: URL ou arquivo inválido!'),
				parse_mode: 'Markdown'
			}
		}
	], {
		cache_time: 0
	}))
	return
}

module.exports = {
	id: 'download',
	name: 'Download',
	about: 'Faz donwload de um arquivo, no formato de .ZIP ou .PDF com menos de 50MB.',
	regex: /^\/download\s(.*)/i,
	example: '/download http://synko.com.br/pdf.pdf',
	classification: ['Ferramentas'],
	plugin,
	inline
}

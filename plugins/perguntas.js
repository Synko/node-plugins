function base (ctx) {
	var input = ctx.match[1]
	var respostas = [
		ctx._('Sim.'),
		ctx._('Não.'),
		ctx._('É certo.'),
		ctx._('É decididamente assim.'),
		ctx._('Sem dúvida.'),
		ctx._('Sim definitivamente.'),
		ctx._('Talvez, sim.'),
		ctx._('Provavelmente.'),
		ctx._('Há perspectiva, é boa.'),
		ctx._('Os sinais apontam para sim.'),
		ctx._('Não conte com isso.'),
		ctx._('Minha resposta é sim.'),
		ctx._('Minha resposta é não.'),
		ctx._('Minhas fontes dizem que sim.'),
		ctx._('Minhas fontes dizem que não.'),
		ctx._('Há perspectiva, não é tão boa.'),
		ctx._('Muito dizem que sim.'),
		ctx._('Muito dizem que não.'),
		ctx._('Absolutamente.'),
		ctx._('Só nos seus sonhos.'),
		ctx._('Há um tempo e lugar para tudo, mas não agora.')
	]
	var gg = [
		ctx._('Pergunte novamente mais tarde.'),
		ctx._('Melhor não dizer agora.'),
		ctx._('Não é possível prever agora.'),
		ctx._('Concentre-se e perguntar de novo.')
	]
	if (input.length < 32) {
		return respostas[Math.floor((Math.random() * respostas.length))]
	} else {
		return gg[Math.floor((Math.random() * gg.length))]
	}
}

function plugin(ctx) {
	return ctx.reply(base(ctx))
}

function inline(ctx) {
	var input = ctx.match[1]
	var basePlugin = base(ctx)
	// #i18n Ed = (Name of bot)
	var output = ctx._`Eu: ${input}\nEd: ${basePlugin}`

	ctx.answerInlineQuery([
		{
			type: 'article',
			title: ctx._('Resultado da resposta.'),
			id: `perguntas`,
			input_message_content: {
				message_text: output,
			}
		}
	], {
		cache_time: 0
	})
}

module.exports = {
	id: 'perguntas',
	name: 'Perguntas',
	about: 'Faça uma pergunta simples ao bot, para obter sua resposta.',
	regex: /^\/pergunta[rs\s]*(.+)/i,
	example: '/perguntas é verdade que uma verdade é verdadeira?',
	classification: ['Entretenimento', 'Ferramentas'],
	plugin,
	inline
}

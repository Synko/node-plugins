function base(ctx) {
	var input = ctx.match[1]
	var table = input
	.replace(/,$/g, '')
	.replace(/,\s/g, ',')
	.split(',')
	var select = table[Math.floor((Math.random() * table.length))]
	return ctx._`Nome soteado foi: <code>${select}</code>`
}

function plugin(ctx) {
	return ctx.replyWithHTML(base(ctx))
}

function inline(ctx) {
	var input = ctx.match[1]
	var basePlugin = base(ctx)
	var output = ctx._`Nomes para sorteio: <code>${input}</code>\n${basePlugin}`
	ctx.answerInlineQuery([
		{
			type: 'article',
			title: ctx._('Resultado do nome soteado.'),
			id: 'sorteio',
			input_message_content: {
				message_text: output,
				parse_mode: 'HTML'
			}
		}
	], {
		cache_time: 0
	})
}

module.exports = {
	id: 'sorteioname',
	name: 'sorteio de Nome',
	about: 'sorteia um nome de uma lista de nomes.',
	regex: /^\/sorteio[s]*\s(.+)/i,
	example: '/sorteio Tiago, Yan, Wesley',
	classification: ['Entretenimento', 'Ferramentas'],
	plugin,
	inline
}

var axios = require('axios')

async function base(ctx) {
	var input = ctx.match[1] || ''
	if (ctx.update.message && ctx.update.message.reply_to_message && ctx.update.message.reply_to_message.text) {
		var replyMsgText = ctx.update.message.reply_to_message.text
		var match = replyMsgText.match(/= (\d*)/)
		if (match && match[1]) {
			input = `${match[1]} ${input}`
		}
	}
	var response = await axios({
		method: 'GET',
		url: 'http://api.mathjs.org/v1/',
		params: {
			expr: input
			//FIX noob in math
			.replace(/x/i, '*')
			.replace(':', '/')
			.replace(' ', '')
		}
	})
	var data = response.data.toString()
	var output = ''
	if (data.match(/error/i)) {
		data = data
		.replace('Invalid left hand side of assignment operator','')
		.replace(/char.*$/i, ctx._('Não é uma expressão matemática.'))
		.replace('Error: ', '')
		.replace('Undefined symbol', ctx._('não foi definido nenhum numero para:'))
		.replace('Syntax = ','')
		.replace('SyntaxValue expected', '')
		return {
			output: ctx._`*Error ao Calcular:* \`${data}\``,
			title: ctx._`Error ao Calcular: ${data}`
		}
	} else {
		data = data.replace('Infinity','Infinito')
		return {
			output: ctx._`*Pelos meus calculos:* \`${input} = ${data}\``,
			title: ctx._`Resultado: ${data}`
		}
	}
}

async function plugin(ctx) {
	//TODO: Add suport reply msg
	var output = await base(ctx)
	return ctx.replyWithMarkdown(output.output)
}

async function inline(ctx) {
	var info = await base(ctx)
	ctx.answerInlineQuery([
		{
			type: 'article',
			title: `${info.title}`,
			id: 'calculadora',
			input_message_content: {
				message_text: info.output,
				parse_mode: 'Markdown'
			}
		}
	], {
		cache_time: 0
	})
}

module.exports = {
	id: 'calc',
	name: 'Calculadora',
	about: 'Plugin de Calculadora',
	regex: /^\/calc[uladora]*\s*(.+)/i,
	example: '/calc 7*1+5',
	classification: ['Ferramentas'],
	plugin,
	inline
}

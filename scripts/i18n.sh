#!/bin/bash
find plugins | grep .js | sort > AllFiles.list
node scripts/getAllInfoText.js
cat AllFiles.list | nodejs-i18n | msgmerge --backup=off --update locales/en.po /dev/stdin
rm -r -f InfoAllPlugin.list AllFiles.list

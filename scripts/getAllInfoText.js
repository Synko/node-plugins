const fs = require('fs')
var plugins = require('../config').plugins
plugins.push('google')

var output = ''
for (var p of plugins) {
    var _ = require(`../plugins/${p}`)
    output += `
// #i18n Name of Plugin (ID: ${_.id})
var text = _('${_.name}')
// #i18n About Text of Plugin (ID: ${_.id})
var text = _('${_.about}')
// #i18n Command example of Plugin (ID: ${_.id})')
var text = _('${_.example.toString().replace(/,/g, ', ')}')`
    for (var c of _.classification) {
        output += `
// #i18n Classification of Plugin
var text = _('${c}')`
    }
}

fs.appendFileSync('AllFiles.list', 'index.js\nInfoAllPlugin.list')
fs.writeFileSync('InfoAllPlugin.list', output)
